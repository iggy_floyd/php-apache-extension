# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project


PROGRAM=$(shell ./config.sh --libso)
HEADERS=$(shell find ./ -iname ".*h")
SRC=$(shell find ./ -iname ".*cc")


all: build
all: $(PROGRAM)



$(PROGRAM): $(HEADERS) $(SRC)
	- ./build.sh


build:
	- ./build.sh


clean:

	./clean.sh

install:

	./install.sh


doc: README.wiki

	- mkdir doc
	- ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	- rm *info
	- ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "sed -e 's/index\.html/{}\.html/g'  {}/index.html > doc/{}.html;  rm -r {}" | sh



test: test/

	- cd test; ./test.py


.PHONY: build clean all doc test install
