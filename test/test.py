#! /usr/bin/env python
# -*- coding: utf-8 -*-


'''
#  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
#  All rights reserved.
#
#
#  Usage: %(scriptName)s
#

'''


from subprocess import Popen,PIPE
import inspect
import sys


sys.path.append('.')


def test_mod(file,port=""):

 return Popen(["curl", "localhost"+port+"/"+file], stdout=PIPE).communicate()[0][:-1]


def test_cpp(file,incs="",args=""):

 cmd="python-config --cflags"
 cmd=cmd.split()
 (pyc,err) = Popen(cmd, stdout=PIPE).communicate()
 pyc=pyc.replace("-Wstrict-prototypes","");
 pyc=pyc.replace("\n","");
 cmd="g++  -std=c++0x  -I. %s   -I/usr/local/include 	   %s -fpic %s.cpp -shared -lboost_python %s -o %s.so"%(pyc,incs,file,args,file)
# print cmd
# return 
 cmd=cmd.split()
 (out,err) = Popen(cmd, stdout=PIPE).communicate()
 import importlib
 mymodule = importlib.import_module(file, package=file)
 
 return getattr(mymodule,"test")()

  




def hello():
   """  hello

   >>> hello()
   'Hello World from FOO'
   """
   return test_mod(str(inspect.stack()[0][3]))

def codeahello():
   """  codeahello

   >>> codeahello()
   'Hello World from Codea: 0'
   """
   return test_mod(str(inspect.stack()[0][3]))

def cplusplushello():
   """  cplusplushello

   >>> cplusplushello()
   'Hello World from CPLUSPLUS 1'
   """
   return test_mod(str(inspect.stack()[0][3]))


def hello_world():
   """  hello_world tests the ngix module hello_world

   >>> hello_world()
   'hello world'
   """
   return test_mod(str(inspect.stack()[0][3]),":9080").replace("\n","")
   
   



if __name__ == '__main__':


    print __doc__ % {'scriptName' : sys.argv[0]}
  
    import doctest
    
    doctest.testmod(verbose=True)
#    doctest.run_docstring_examples(boostsplit, globals())

