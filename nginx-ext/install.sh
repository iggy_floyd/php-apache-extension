#!/bin/bash
#
#  function to install the apache2 module
#
#


function  install() {

  local  ngx_src_dir="../nginx-1.6.0"
  local  ngx_install_dir="/usr/local/nginx"
  cd ${ngx_src_dir}
  make install
  cd -

  for i in `find src/ -iname "*.c"  | sort -h -r`
  do

     modname=`echo $i | sed -e 's/\(src\/ngx_http_\)\(.*\)\(_module\.c\)/\2/g'`
     echo $modname

     local location="	location = /${modname} {  ${modname};  }     "
     sed  '/#_new_modules_here/a '"$location"'' ${ngx_install_dir}/conf/nginx.conf > ${ngx_install_dir}/conf/nginx.conf.new; mv ${ngx_install_dir}/conf/nginx.conf.new ${ngx_install_dir}/conf/nginx.conf



 done


 ${ngx_install_dir}/sbin/nginx -s stop
 ${ngx_install_dir}/sbin/nginx

return 0;


}

install
