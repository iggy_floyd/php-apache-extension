#!/bin/bash

  program=`basename \`pwd\``
  program_src=`pwd`/src
  
  ngx_dir="../nginx-1.6.0"
  cd ${ngx_dir}
  
  ./configure --add-module=${program_src}
   make
   cd -
  
