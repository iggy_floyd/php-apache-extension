/* ====================================================================
 * The C++ Object-oriented Development Environment for Apache (Codea)
 * Open Source Project License Version 1.0
 *
 * Copyright (c) 2001 The Codea Open Source Project.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Codea Open Source Project and is based upon software
 *        written by the Apache Software Foundation.  Codea is
 *        not a derivative work of Apache nor is Codea endorsed
 *        by the Apache Software Foundation."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Codea" and "Codea Open Source Project" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact the project administrator.
 *
 * 5. Products derived from this software may not be called "Codea",
 *    nor may "Codea" appear in their name, without prior written
 *    permission.
 *
 * 6. All conditions set forth in the Apache Software Foundation
 *    license must be met in addition to those set forth in this
 *    license agreement.  For details regarding the Apache Software
 *    Foundation license, please see http://www.apache.org/LICENSE.txt.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

#ifndef __CODEA_HOOKS_H__
#define __CODEA_HOOKS_H__

/**
 * @file codea_hooks.h
 * @brief Specify code for CodeaHooks class and implementation.
 */

#include <string>
#include <map>

#include "codea_core.h"

/**
 * Base class for Codea modules.
 *
 * This class includes virtual functions with default behavior for all hookpoints.
 * (Well, all except ap_hook_create_request, which I don't know how to do yet. -MR)
 * CodeaHooks subclasses may use the base methods to perform default behavior.
 */
class CodeaHooks
{
public:
	typedef std::string tag_t; /**< The type used for an identifying tag in a CodeaHooks subclass. */

	/** Virtual destructor for base class. */
	virtual ~CodeaHooks() {};

	/** Default functionality for post_config (does nothing). */
	virtual void _Base_Post_Config( apr_pool_t* pconf, apr_pool_t* plog, apr_pool_t* ptmp, server_rec* s ) {}
	/** Default functionality for open_logs (does nothing). */
	virtual void _Base_Open_Logs( apr_pool_t* pconf, apr_pool_t* plog, apr_pool_t* ptmp, server_rec* s )   {}

	/** Default functionality for child_init (does nothing). */
	virtual void _Base_Child_Init( apr_pool_t* pchild, server_rec* s ) {}

	/** Default functionality for insert_filter (does nothing). */
	virtual void _Base_Insert_Filter( request_rec* r ) {}

	/** Default functionality for optional_fn_retrieve (does nothing). */
	virtual void _Base_Optional_Fn_Retrieve( void ) {}

	/** Default functionality for http_method.  Returns the method from the request_rec. */
	virtual const char* _Base_Http_Method( const request_rec* r )
	{
		if ( r )
		{
			return r->method;
		}
		return 0;
	}

	/** Default functionality for default_port.  Returns the server port from the request_rec, or DEFAULT_HTTP_PORT if unknown. */
	virtual apr_port_t _Base_Default_Port( const request_rec* r )
	{
		if ( r )
		{
			return r->server->port;
		}
		return DEFAULT_HTTP_PORT; // guess
	}

	/** Default functionality for auth_checker.  Returns DECLINED. */
	virtual int _Base_Auth_Checker( request_rec* r )      { return DECLINED; }
	/** Default functionality for access_checker .  Returns DECLINED. */
	virtual int _Base_Access_Checker( request_rec* r )    { return DECLINED; }
	/** Default functionality for check_user_id .  Returns DECLINED. */
	virtual int _Base_Check_User_ID( request_rec* r )     { return DECLINED; }
	// Using this method gives us problems;
	// disable it for now.  -MR
	// virtual int _Base_Create_Request( request_rec* r );
	/** Default functionality for fixups.  Returns DECLINED. */
	virtual int _Base_Fixups( request_rec* r )            { return DECLINED; }
	/** Default functionality for handler.  Returns DECLINED. */
	virtual int _Base_Handler( request_rec* r )           { return DECLINED; }
	/** Default functionality for header_parser.  Returns DECLINED. */
	virtual int _Base_Header_Parser( request_rec* r )     { return DECLINED; }
	/** Default functionality for log_transaction.  Returns DECLINED. */
	virtual int _Base_Log_Transaction( request_rec* r )   { return DECLINED; }
	/** Default functionality for post_read_request.  Returns DECLINED. */
	virtual int _Base_Post_Read_Request( request_rec* r ) { return DECLINED; }
	/** Default functionality for quick_handler.  Returns DECLINED. */
	virtual int _Base_Quick_Handler( request_rec* r )     { return DECLINED; }
	/** Default functionality for translate_name.  Returns DECLINED. */
	virtual int _Base_Translate_Name( request_rec* r )    { return DECLINED; }
	/** Default functionality for type_checker.  Returns DECLINED. */
	virtual int _Base_Type_Checker( request_rec* r )      { return DECLINED; }

	/** Default functionality for pre_connection.  Returns DECLINED. */
	virtual int _Base_Pre_Connection( conn_rec* c )     { return DECLINED; }
	/** Default functionality for process_connection.  Returns DECLINED. */
	virtual int _Base_Process_Connection( conn_rec* c ) { return DECLINED; }
};

class CodeaHookCreator; // forward decl.

/**
 * @brief Registry of CodeaHookCreator subclasses.
 * @see CodeaHookCreator, CodeaHooks::tag_t
 *
 * This class manages a registry of CodeaHookCreator subclasses.  Subclass
 * instance pointers are registered with the registry along with a tag
 * name as a key.  The instance pointer can later be retrieved from the
 * registry by the tag name.
 *
 * This class is a singleton.  Consumers of this class never need to
 * obtain an instance of the class; instead they access the singleton
 * instance via static class methods.
 */
class CodeaHookRegistry
{
private:
	typedef std::map< CodeaHooks::tag_t, const CodeaHookCreator* > registry_map_t; /**< Type used to map tag names to creator instances. */
	typedef registry_map_t::iterator registry_iterator_t;                          /**< Type used to iterate through the registry map. */
	typedef registry_map_t::const_iterator registry_const_iterator_t;              /**< Type used to iterate read-only through the registry map. */

	registry_map_t registry; /**< The registry of creators. */

	static CodeaHookRegistry* pSingletonInstance; /**< Singleton instance pointer. */

	/** Constructor - private to preserve Singleton behavior. */
	CodeaHookRegistry()
	{}

	/** Retrieve (create if necessary) the Singleton instance. */
	static CodeaHookRegistry* Instance()
	{
		return pSingletonInstance ? pSingletonInstance : (pSingletonInstance = new CodeaHookRegistry);
	}
public:
	/**
	 * @brief Create an entry in the registry.
	 * @param tag - The tag to be used as the registry key.
	 * @param pCreator - pointer to a CodeaHookCreator subclass instance that will be the registry entry.
	 */
	static void MakeEntry( const CodeaHooks::tag_t& tag, const CodeaHookCreator* pCreator )
	{
		CodeaHookRegistry* pInst = CodeaHookRegistry::Instance();
		pInst->registry.insert( make_pair( tag, pCreator ) );
	}
	/**
	 * @brief Retrieve an entry from the registry by tag.
	 * @param tag - The tag to use as the key for looking up the entry.
	 * @return Pointer to the CodeaHookCreator subclass entry that matches the tag, or NULL/0 if not found.
	 */
	static const CodeaHookCreator* GetEntry( const CodeaHooks::tag_t& tag )
	{
		CodeaHookRegistry* pInst = CodeaHookRegistry::Instance();
		registry_const_iterator_t pMatch = pInst->registry.find( tag );
		if( pInst->registry.end() != pMatch )
		{
			return pMatch->second;
		}
		return 0;
	}
};

/**
 * @brief Base class for CodeaHooks subclass creators.
 * @see CodeaHooks, CODEA_REGISTER_HOOK_CLASS
 *
 * The CodeaHookCreator is a base class that is automatically
 * subclassed when the CODEA_REGISTER_HOOK_CLASS macro is used.
 * The purposes of this class are:
 *   - Create a CodeaHooks subclass pointer and manage it
 *   - Return the pointer when asked
 *   - Delete the pointer when the creator is destroyed
 *
 * This is an abstract base class.
 */
class CodeaHookCreator
{
protected:
	mutable CodeaHooks* pInstance; /**< Pointer to the CodeaHooks subclass instance */
public:
	/**
	 * @brief Create a CodeaHookCreator instance, and register this instance with the tag in the CodeaHookRegistry
	 * @param tag - The tag to be used as the key for this creator
	 * @see CodeaHookRegistry, CodeaHooks::tag_t
	 */
	CodeaHookCreator( const CodeaHooks::tag_t& tag )
		:
	pInstance( 0 )
	{
		CodeaHookRegistry::MakeEntry( tag, this );
	}
	/** Destructor - deletes the CodeaHooks instance if it exists. */
	virtual ~CodeaHookCreator()
	{
		if( pInstance ) delete pInstance;
	}

	/** Pure virtual method, overridden by subclasses, to return the proper CodeaHooks subclass instance. */
	virtual CodeaHooks* ReturnInstance() const =0;
};

/** Factory to create/retrieve CodeaHooks subclass instances by tag. */
class CodeaHookFactory
{
public:
	/**
	 * @brief Retrieve the CodeaHooks subclass instance that corresponds to the tag.
	 * @param tag - The tag that corresponds to the CodeaHooks subclass instance we want
	 * @return CodeaHooks subclass pointer from creator in registry, or NULL/0 if not found.
	 * @see CodeaHooks, CodeaHooks::tag_t, CodeaHookCreator, CodeaHookRegistry
	 *
	 * How this function works with the registry and creator to retrieve the CodeaHooks instance:
	 *   - Using the supplied tag, the factory requests the creator entry from the registry.
	 *   - The registry looks up the creator that matches the tag and returns it
	 *   - The factory uses the creator to get the instance if the creator is valid
	 *   - The creator creates/returns the managed CodeaHooks subclass instance
	 */
	static CodeaHooks* GetInstance( const CodeaHooks::tag_t& tag )
	{
		const CodeaHookCreator* pCreator = CodeaHookRegistry::GetEntry( tag );
		return pCreator ? pCreator->ReturnInstance() : 0;
	}
};

/**
 * @brief Register a CodeaHooks subclass with Codea.
 * @param class_name - The name of the CodeaHooks subclass (no quotes)
 * @param class_id - The identifier (tag) for the CodeaHooks subclass
 * @see CodeaHooks, CodeaHookCreator
 *
 * This macro automatically creates the CodeaHookCreator subclass
 * for the specified class_name.  It automatically creates the
 * ReturnInstance method within the class correctly.  And, it
 * automatically registers the creator and tag with the registry.
 *
 * Usage:
 *
 * CODEA_REGISTER_HOOK_CLASS( MyCodeaHooks, MyCodeaHooks::GetTag() );
 */
#define CODEA_REGISTER_HOOK_CLASS( class_name, class_id ) \
class class_name##_CodeaHookCreator : public CodeaHookCreator { \
public: \
class_name##_CodeaHookCreator( const CodeaHooks::tag_t& tag ) : CodeaHookCreator( tag ) { } \
virtual CodeaHooks* ReturnInstance() const { \
return pInstance ? pInstance : pInstance = new class_name; \
} \
} class_name##_creator( class_id )

/**
 * @brief Retrieve a CodeaHooks subclass instance by tag from the CodeaHookFactory.
 * @param pInst - pointer to the CodeaHooks subclass instance where the retrieved instance will be stored.
 * @param tag - tag for this CodeaHooks subclass.
 *
 * Usage:
 *
 * MyCodeaHooks* pmch = CODEA_RETRIEVE_HOOK_CLASS( pmch, MyCodeaHooks::GetTag() );
 */
template < class _t >
_t* CODEA_RETRIEVE_HOOK_CLASS( _t* pInst, const CodeaHooks::tag_t& tag )
{
	return pInst = dynamic_cast<_t*>(CodeaHookFactory::GetInstance( tag ));
}

#endif // __CODEA_HOOKS_H__
