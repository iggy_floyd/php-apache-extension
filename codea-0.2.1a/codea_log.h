/* ====================================================================
00002  * The C++ Object-oriented Development Environment for Apache (Codea)
00003  * Open Source Project License Version 1.0
00004  *
00005  * Copyright (c) 2001 The Codea Open Source Project.  All rights
00006  * reserved.
00007  *
00008  * Redistribution and use in source and binary forms, with or without
00009  * modification, are permitted provided that the following conditions
00010  * are met:
00011  *
00012  * 1. Redistributions of source code must retain the above copyright
00013  *    notice, this list of conditions and the following disclaimer.
00014  *
00015  * 2. Redistributions in binary form must reproduce the above copyright
00016  *    notice, this list of conditions and the following disclaimer in
00017  *    the documentation and/or other materials provided with the
00018  *    distribution.
00019  *
00020  * 3. The end-user documentation included with the redistribution,
00021  *    if any, must include the following acknowledgment:
00022  *       "This product includes software developed by the
00023  *        Codea Open Source Project and is based upon software
00024  *        written by the Apache Software Foundation.  Codea is
00025  *        not a derivative work of Apache nor is Codea endorsed
00026  *        by the Apache Software Foundation."
00027  *    Alternately, this acknowledgment may appear in the software itself,
00028  *    if and wherever such third-party acknowledgments normally appear.
00029  *
00030  * 4. The names "Codea" and "Codea Open Source Project" must
00031  *    not be used to endorse or promote products derived from this
00032  *    software without prior written permission. For written
00033  *    permission, please contact the project administrator.
00034  *
00035  * 5. Products derived from this software may not be called "Codea",
00036  *    nor may "Codea" appear in their name, without prior written
00037  *    permission.
00038  *
00039  * 6. All conditions set forth in the Apache Software Foundation
00040  *    license must be met in addition to those set forth in this
00041  *    license agreement.  For details regarding the Apache Software
00042  *    Foundation license, please see http://www.apache.org/LICENSE.txt.
00043  *
00044  * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
00045  * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
00046  * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
00047  * DISCLAIMED.  IN NO EVENT SHALL THE CODEA OPEN SOURCE PROJECT OR
00048  * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
00049  * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
00050  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
00051  * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
00052  * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
00053  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
00054  * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
00055  * SUCH DAMAGE.
00056  * ====================================================================
00057  */
 #ifndef __CODEA_LOG_H__
 #define __CODEA_LOG_H__
 #include "codea_core.h"

 class CodeaLog
 {
 public:
         typedef enum
         {
                 SUPPRESS_SYSTEM_ERROR,
                 DISPLAY_SYSTEM_ERROR
         } syserr_flag_t;
 private:
         typedef enum 
         {
                 POOL,    
                 SERVER,  
                 REQUEST, 
                 UNKNOWN  
         } memorysource_t;
 
         memorysource_t mem_source; 
         union
         {
                 apr_pool_t* m_pPool;
                 server_rec* m_pSvr;
                 request_rec* m_pReq;
         };
 
 public:
         CodeaLog( apr_pool_t* ppool )
                 :
                 mem_source( POOL ),
                 m_pPool( ppool )
         {}
 
         CodeaLog( server_rec* psvr )
                 :
                mem_source( SERVER ),
                 m_pSvr( psvr )
         {}
 
         CodeaLog( request_rec* preq )
                 :
                 mem_source( REQUEST ),
                 m_pReq( preq )
         {}
 
         void Emerg( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
         { operator()( msg, showSysErr, APLOG_EMERG ); }
         void Alert( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
         { operator()( msg, showSysErr, APLOG_ALERT ); }
         void Crit( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
         { operator()( msg, showSysErr, APLOG_CRIT ); }
        void Err( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
       { operator()( msg, showSysErr, APLOG_ERR ); }
         void Warning( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
         { operator()( msg, showSysErr, APLOG_WARNING ); }
         void Notice( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
         { operator()( msg, showSysErr, APLOG_NOTICE ); }
         void Info( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
         { operator()( msg, showSysErr, APLOG_INFO ); }
         void Debug( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR )
         { operator()( msg, showSysErr, APLOG_DEBUG ); }
 
         void operator() ( const std::string& msg, syserr_flag_t showSysErr=SUPPRESS_SYSTEM_ERROR, int severity=APLOG_ERR )
         {
                 int _severity = showSysErr==SUPPRESS_SYSTEM_ERROR ? severity|APLOG_NOERRNO : severity;
                 switch( mem_source )
                 {
                 case POOL:
                         ap_log_perror( APLOG_MARK, _severity, 0, m_pPool, msg.c_str() );
                         break;
                 case SERVER:
                         ap_log_error( APLOG_MARK, _severity, 0, m_pSvr, msg.c_str() );
                         break;
                 case REQUEST:
                         ap_log_rerror( APLOG_MARK, _severity, 0, m_pReq, msg.c_str() );
                         break;
                 default:
                         break;
                }
        }
};
#endif // __CODEA_LOG_H__
