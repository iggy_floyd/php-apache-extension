#!/bin/bash
#

readoptions() {

local mode;


while [[ $# > 0 ]]
do

	local key="$1"
	shift

	case $key in
	--inc)
	    mode="inc"
            echo $mode
	    return 0
	    ;;
    	--libs)
    	    mode="libs"
            echo $mode
            return 0
            ;;
    	--libso)
    	    mode="libso"
            echo $mode
            return 0
            ;;
             *)
            echo "usage: $0 [--inc]"
            echo "       $0 [--libs]"
            echo "       $0 [--libso]"
            return 1
            ;;
esac
done

echo "usage: $0 [--inc]"
echo "       $0 [--libs]"
echo "       $0 [--libso]"

return 1
}



configurepring() {

# sub-packages under considerations
local packages="(apache-ext)"

# additional compiler and linker options 
local additionallibs="-ldl -lpthread  -lz -lm -lrt"


local dir=`dirname  $0`;
[[ ${dir[0]} != "/" ]] && dir=`pwd`/$dir

# the final output *.so library
local program=`basename \`pwd\``  
local libso=$dir/lib${program}.so


dir=`find "$dir" -iname "*.so" | egrep $packages  | sort -h -r | xargs -I {} echo {}`

local inc_echo
local libs_echo
local libs_file_echo

for i in $dir
 do 
	local _dir_inc=`dirname $i | sed -e 's/lib$/include/g'`
	local _dir_libs=`dirname $i`
	local _file_libs=-l`basename $i | sed -e 's/\.so//g'`
	inc_echo=`echo "$inc_echo -I$_dir_inc"`
        libs_echo=`echo "$libs_echo -L$_dir_libs"`
        libs_file_echo=`echo "$libs_file_echo $_file_libs"`
 done


# mysql support
#[[ `which mysql_config` ]] && [[ `echo $libs_echo | grep mysql` ]]  && 
#mysql_inc=`mysql_config --include` && 
#mysql_libs=`mysql_config --libs | sed -ne 's/\(.*\)\(-L[^ ]*\) \(-lmysql[^ ]*\) \(.*\)/\2/p'` &&
#mysql_file=`mysql_config --libs | sed -ne 's/\(.*\)\(-L[^ ]*\) \(-lmysql[^ ]*\) \(.*\)/\3/p'`


#inc_echo=`echo "$inc_echo $mysql_inc"`
#libs_echo=`echo "$libs_echo $mysql_libs"`



#libs_file_echo=" -lsoci_sqlite3  -lsoci_mysql -lsoci_core -lsqlite $mysql_file -lbktreesqlite3   "
#libs_echo=`echo "-Wl,--whole-archive  $libs_echo  $libs_file_echo -Wl,--no-whole-archive $additionallibs"`
libs_echo=`echo  $libs_echo  $libs_file_echo `


[[ $# == 1 ]] && [[ "$1" == "inc" ]] && [[ -n "$inc_echo" ]] && echo "$inc_echo"  && return 0;
[[ $# == 1 ]] && [[ "$1" == "libs" ]] && [[ -n "$libs_echo" ]] && echo "$libs_echo" && return 0;
[[ $# == 1 ]] && [[ "$1" == "libso" ]] && echo "$libso" && return 0;

return 1;
}




# the main program starts here
regime=`readoptions "$@"`
[[ $? > 0  ]] && echo "$regime" && exit $?
configurepring $regime

