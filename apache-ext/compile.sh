#!/bin/bash

  program=`basename \`pwd\``  

  apache_inc="-I/usr/include/httpd -I/usr/include/apr-1.0 -I/usr/local/include -I/usr/include -I/usr/include/apache2/"

# method 1: using apxs2
#  for i in `find src/ -iname "*.cc" | sort -h -r`; do apxs2 -a  -Iinclude  -c ${i}; done


# method 2: using g++ 
# for modules which are build without codea technology and cplusplus
  for i in `find src/ -iname "*.cc" | grep -v codea | sort -h -r`; do g++ -g  -fPIC -shared  -Iinclude  $apache_inc -c ${i}; done

  [[ `ls *.o |  grep -v codea | grep -v cplusplus 2> /dev/null` ]] && ( ls *.o | grep -v codea  | sed -e 's/\.o//g' |  xargs -I {}  echo "gcc -shared -o {}.so  {}.o;   mv  {}.so lib" | bash)


# for modules which are built using codea technology
  [[ `which apxs2` ]] && APXS_CFLAGS=`apxs2 -q CFLAGS`   
  [[ `which apxs2` ]] && APXS_CFLAGS_SHLIB=`apxs2 -q CFLAGS_SHLIB`   
  [[ `which apxs2` ]] && APXS_INCLUDEDIR=`apxs2 -q INCLUDEDIR`   

  codea_path="../codea-0.2.1a"
  codea_inc="-I${codea_path} -I. -I${APXS_INCLUDEDIR} -iquote ${LOCAL_INCLUDES}"
  codea_flags="-Wall -fPIC ${APXS_CFLAGS} ${APXS_CFLAGS_SHLIB} ${LOCAL_CFLAGS}"
  codea_cpp="${codea_path}/codea_hooks.cpp"
  codea_o="${codea_path}/codea_hooks.o"


  for i in `find src/ -iname "*.cc" | grep  codea | sort -h -r`; do    g++  ${codea_flags} -Iinclude  ${codea_inc} ${codea_cpp} -c ${i}; done
  [[ `ls *.o | grep  codea  2> /dev/null` ]] && ( ls *.o | grep codea  | sed -e 's/\.o//g' |  xargs -I {}  echo "gcc -shared -o {}.so  {}.o ${codea_o};   mv  {}.so lib" )
  [[ `ls *.o | grep  codea  2> /dev/null` ]] && ( ls *.o | grep codea  | sed -e 's/\.o//g' |  xargs -I {}  echo "gcc -shared -o {}.so  {}.o ${codea_o};   mv  {}.so lib" | bash)



  cplusplus_path="../mod_cplusplus"
  cplusplus_inc="-I${cplusplus_path}/include -I. -I${APXS_INCLUDEDIR} -iquote ${LOCAL_INCLUDES}"
  cplusplus_flags="-Wall -fPIC ${APXS_CFLAGS} ${APXS_CFLAGS_SHLIB} ${LOCAL_CFLAGS}"
  cplusplus_cpp=
  cplusplus_o="${cplusplus_path}/src/.libs/libmod_cplusplus.so"


 for i in `find src/ -iname "*.cc" | grep  cplusplus | sort -h -r`; do    g++  ${cplusplus_flags} -Iinclude  ${cplusplus_inc} ${cplusplus_cpp} ${cplusplus_o} -c ${i}; done
 [[ `ls *.o | grep  cplusplus  2> /dev/null` ]] && ( ls *.o | grep cplusplus  | sed -e 's/\.o//g' |  xargs -I {}  echo "gcc -shared -o {}.so  {}.o ${cplusplus_o}; mv {}.so lib" )
 [[ `ls *.o | grep  cplusplus  2> /dev/null` ]] && ( ls *.o | grep cplusplus  | sed -e 's/\.o//g' |  xargs -I {}  echo "gcc -shared -o {}.so  {}.o ${cplusplus_o}; mv {}.so lib" | bash)
