/* 
 * File:   mod_cplusplushello.h
 * Author: debian
 *
 * Created on November 7, 2014, 12:01 PM
 */

#ifndef MOD_CPLUSPLUSHELLO_H
#define	MOD_CPLUSPLUSHELLO_H


#include "apache_filters.h"

class CplusplusHelloHandler : public ApacheHandler
{
private:
    unsigned int mHits;
public:
    CplusplusHelloHandler(ApacheServerRec *pServer);
    ~CplusplusHelloHandler(void);
    int handler(ApacheRequestRec *pRequest);
};



#endif	/* MOD_CPLUSPLUSHELLO_H */

