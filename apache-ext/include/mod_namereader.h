/* 
 * File:   mod_namereader.h
 * Author: debian
 *
 * Created on November 6, 2014, 4:33 PM
 */

#ifndef MOD_NAMEREADER_H
#define	MOD_NAMEREADER_H

#define MODULE_HOOKS namereader_hooks
#define MODULE   namereader_module

#ifdef __cplusplus
#define EXTERN_C_BLOCK_BEGIN    extern "C" {
#define EXTERN_C_BLOCK_END      }
#define EXTERN_C_FUNC           extern "C"
#else
#define EXTERN_C_BLOCK_BEGIN
#define EXTERN_C_BLOCK_END
#define EXTERN_C_FUNC
#endif

#include <httpd.h>
#include <http_core.h>
#include <http_protocol.h>
#include <http_config.h>
#include <http_request.h>
#include "http_connection.h"


#include "apr_strings.h"
#include "apr_network_io.h"
#include "apr_md5.h"
#include "apr_sha1.h"
#include "apr_hash.h"
#include "apr_base64.h"
#include "apr_dbd.h"
#include <apr_file_info.h>
#include <apr_file_io.h>
#include <apr_tables.h>

#include "ap_config.h"



class NameReader
{
private:

    request_rec*    m_pRequestRec;


	
public:
	
    NameReader( request_rec* inpRequestRec ) :
        m_pRequestRec( inpRequestRec )
    { 
        
             ap_rputs( "I have been  created", m_pRequestRec );
        
        }
        
        ~NameReader() { 
        
          ap_rputs( "I have been  destroyed", m_pRequestRec );
        }


typedef struct {
    const char* key;
    const char* value;
} keyValuePair;
    

keyValuePair* readPost() {} 
int RunHandler();
};


#endif	/* MOD_NAMEREADER_H */

