#include "mod_codeahello.h"


#include "codea_hookmap.h"
#include "codea_hooks.h"
#include "codea_log.h"
//#include "codea_hooks.cpp"



static const CodeaHooks::tag_t& CodeaHello_tag( "CodeaHello" );

/**
 * This simple CodeaHooks subclass defines only a content handler to verify
 * that we have invoked the module if the uri is equivalent to /codea_simple.
 */
class CodeaHelloHooks : public CodeaHooks
{
public:
        
        static int mHits;
	static const tag_t& GetTag() { return CodeaHello_tag; }

	static int HelloHandler( request_rec* r )
	{
		CodeaLog log( r );
		log.Info( "SimpleHandler called." );
		if ( 0 == ::strcmp( r->uri, "/codeahello" ) )
		{
                     char buf[64];
                     sprintf(buf,"Hello World from Codea: %d\n",mHits++);
			ap_rputs( buf, r );
			return OK;
		}
		return DECLINED;
	}
};

int CodeaHelloHooks::mHits=0;
CODEA_REGISTER_HOOK_CLASS( CodeaHelloHooks, CodeaHelloHooks::GetTag() );

CODEA_BEGIN_HOOK_MAP( codeahello )
CODEA_HANDLER( CodeaHelloHooks::HelloHandler, 0, 0, APR_HOOK_MIDDLE )
CODEA_END_HOOK_MAP( codeahello )

CODEA_PUBLISH_MODULE( codeahello, 0, 0, 0, 0, 0 )

        
        
