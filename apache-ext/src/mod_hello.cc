
#include "mod_hello.h"


EXTERN_C_FUNC
int hello_handler( request_rec* inpRequest )
{
    int nReturnVal = DECLINED;
	
    if ( inpRequest->handler != NULL && strcmp( inpRequest->handler, "hello" ) == 0 )
    {
        ap_rputs( "Hello World from FOO\n", inpRequest );
        nReturnVal = OK;
    }

    return nReturnVal;
}

EXTERN_C_FUNC
void hello_hooks( apr_pool_t* inpPool )
{
    ap_hook_handler( hello_handler, NULL, NULL, APR_HOOK_MIDDLE );
}

EXTERN_C_BLOCK_BEGIN
module AP_MODULE_DECLARE_DATA hello_module =
{
    STANDARD20_MODULE_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    hello_hooks
};
EXTERN_C_BLOCK_END