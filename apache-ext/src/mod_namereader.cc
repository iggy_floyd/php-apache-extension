#include "mod_namereader.h"

// RunHandler of the NameReader
int
NameReader::RunHandler()
{
    int nReturnVal = DECLINED;
	
    if ( m_pRequestRec->handler != NULL && strcmp( m_pRequestRec->handler, "namereader" ) == 0 )
    {
        
        //apr_table_t*GET; 
        //apr_array_header_t*POST; 

        //ap_args_to_table(m_pRequestRec, &GET); 
        //ap_parse_form_data(m_pRequestRec, NULL, &POST, -1, 8192);
        
        ap_rputs( "Hello World from FOO", m_pRequestRec );
        nReturnVal = OK;
    }

    return nReturnVal;
}




/* Custom definition to hold any configuration data we may need.
   At this stage we just use it to keep a copy of the CApplication
   object pointer. Later we will add more when we need specific custom
   configuration information. */
EXTERN_C_BLOCK_BEGIN
typedef struct
{
    void* vpNameReader;
} 
CONFIG_t;
EXTERN_C_BLOCK_END

       
/* Forward reference to our custom function to save the FOOCONFIG_t* 
   configuration pointer with Apache. */
EXTERN_C_FUNC 
void _register_config_ptr( request_rec* inpRequest, CONFIG_t* inpConfig );

/* Forward reference to our custom function to get the FOOCONFIG_t* 
   configuration pointer when we need it. */
EXTERN_C_FUNC 
CONFIG_t* _get_config_ptr( request_rec* inpRequest );

/* Custom function to ensure our NameReader get's deleted at the
   end of the request cycle. */
EXTERN_C_FUNC
apr_status_t _delete_capplication_object( void* inPtr )
{
    if ( inPtr )
        delete ( NameReader* )inPtr;
	
    return OK;
}


/* Our custom handler (content generator) 
   */
EXTERN_C_FUNC
int _handler( request_rec* inpRequest )
{
    /* Create an instance of our application. */
    NameReader* pApp = new NameReader( inpRequest );
	
    if ( pApp == NULL )
	    return HTTP_SERVICE_UNAVAILABLE;
		    
    /* Register a C function to delete pApp
       at the end of the request cycle. */
    apr_pool_cleanup_register( 
        inpRequest->pool, 
        ( void* )pApp, 
        _delete_capplication_object, 
        apr_pool_cleanup_null 
    );
		
    /* Reserve a temporary memory block from the
       request pool to store data between hooks. */
    CONFIG_t* pConfig = 
        ( CONFIG_t* ) apr_palloc( 
            inpRequest->pool, sizeof( CONFIG_t ) );
		
    /* Remember our application pointer for future calls. */
    pConfig->vpNameReader = ( void* )pApp;
		
    /* Register our config data structure for our module. */
    _register_config_ptr( inpRequest, pConfig );
		
    /* Run our application handler. */
    return pApp->RunHandler();
}






/* Apache callback to register our hooks.
   */
EXTERN_C_FUNC
void MODULE_HOOKS( apr_pool_t* inpPool )
{
    ap_hook_handler( _handler, NULL, NULL, APR_HOOK_MIDDLE );
}

/* Our standard module definition.
   */
EXTERN_C_BLOCK_BEGIN
module AP_MODULE_DECLARE_DATA MODULE =
{
    STANDARD20_MODULE_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    MODULE_HOOKS
};
EXTERN_C_BLOCK_END




/* Custom function to register our FOOCONFIG_t* pointer with Apache
   for retrieval later as required. */
EXTERN_C_FUNC
void _register_config_ptr( request_rec* inpRequest, CONFIG_t* inpPtr )
{
    ap_set_module_config( inpRequest->request_config, &MODULE, ( void* )inpPtr );
}


/* Custom function to retrieve our FOOCONFIG_t* pointer previously
   registered with Apache on this request cycle. */
EXTERN_C_FUNC
CONFIG_t* _get_config_ptr( request_rec* inpRequest )
{
    CONFIG_t* pReturnValue = NULL;
	
    if ( inpRequest != NULL )
    {
        pReturnValue = 
            ( CONFIG_t* )ap_get_module_config( 
                inpRequest->request_config, &MODULE);
    }
	
    return pReturnValue;
}
