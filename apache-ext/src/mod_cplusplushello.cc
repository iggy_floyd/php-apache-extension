
#include "mod_cplusplushello.h"

CplusplusHelloHandler::CplusplusHelloHandler(ApacheServerRec *pServer)
:    ApacheHandler(pServer)
{
    mHits = 0;
    ap_log_error(APLOG_MARK, APLOG_NOERRNO|APLOG_ERR, 0, NULL,
                 "constructing mod_cplusplus handler.");
}

CplusplusHelloHandler::~CplusplusHelloHandler()
{
    ap_log_error(APLOG_MARK, APLOG_NOERRNO|APLOG_ERR, 0, NULL,
                 "destroying mod_cplusplus handler.");
}

int CplusplusHelloHandler::handler(ApacheRequestRec *pRequest)
{
    mHits++;
    //pRequest->content_type("application/x-httpd-cgi");
    //pRequest->dump();
    pRequest->rprintf("Hello World from CPLUSPLUS %d\n",mHits);
    ap_setup_client_block(pRequest->get_request_rec(), REQUEST_CHUNKED_ERROR);
    return OK;
}

/*=====================INSTANCIATORS==========================*/

ApacheHandler *instanciate_test(ApacheServerRec *pServer)
{
    return new CplusplusHelloHandler(pServer);
}

cpp_factory_t cplusplushello_handler = {
    instanciate_test,
    NULL,
    NULL,
    NULL,
};