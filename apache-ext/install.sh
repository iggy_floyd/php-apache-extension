#!/bin/bash
#
#  function to install the apache2 module
#
#


function  install() {
  local program=`basename \`pwd\``  
  local libstdc="LoadFile /usr/lib/i386-linux-gnu/libstdc++.so.6"
  local cplusplus="LoadFile "`pwd`/../mod_cplusplus/src/.libs/libmod_cplusplus.so

  [[ -z `which apxs2` ]] && echo "Please, install, apache2-threaded-dev" &&  return 1;
  [[ -z `/usr/sbin/apache2 -V  2> /dev/null` ]] && echo "Please, install, apache2"  &&  return 1;

  local conf_dir=`/usr/sbin/apache2 -V | grep HTTPD_ROOT | sed -e 's/\(.*\)=\"\(.*\)\"/\2/g'`
  local module_dir=`/usr/sbin/apache2 -V | grep SUEXEC_BIN | sed -e 's/\(.*\)=\"\(.*\)\"/\2/g'`
  module_dir=`dirname $module_dir`/modules

  local mod_cplusplus_so=`find \`pwd\`/.. -iname "libmod_cplusplus.so"`


  for i in `find lib/ -iname "*.so"  | sort -h -r`
  do
	 modname=`echo $i | sed -e 's/\(lib\/mod_\)\(.*\)\.so/\2/g'`
	 apxs2 -n $modname -i $i
	_load=
         [  x`echo $modname | grep codea` != "x"  ] &&  _load=$libstdc;
         [  x`echo $modname | grep cplusplus` != "x"  ] &&  _load=$cplusplus;

if [ x`echo $modname | grep cplusplus` == "x"  ]
then
         cat << _END > ${conf_dir}/mods-enabled/${modname}.conf 
<Location /${modname}>
SetHandler $modname
</Location>
_END

        cat << _END >  ${conf_dir}/mods-enabled/${modname}.load 
$_load
LoadModule ${modname}_module ${module_dir}/mod_${modname}.so
_END

else

	cat << _END > ${conf_dir}/mods-enabled/${modname}.conf 
<Location /${modname}>
AddCPPHandler ${modname}_handler
</Location>
_END


        cat << _END >  ${conf_dir}/mods-enabled/${modname}.load 
LoadModule cplusplus_module ${mod_cplusplus_so}
LoadCPPHandler ${modname}_handler ${module_dir}/mod_${modname}.so
_END

fi

  done

return 0;


}

install
